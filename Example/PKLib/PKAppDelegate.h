//
//  PKAppDelegate.h
//  PKLib
//
//  Created by 许良伟 on 09/11/2018.
//  Copyright (c) 2018 许良伟. All rights reserved.
//

@import UIKit;

@interface PKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
