//
//  main.m
//  PKLib
//
//  Created by 许良伟 on 09/11/2018.
//  Copyright (c) 2018 许良伟. All rights reserved.
//

@import UIKit;
#import "PKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PKAppDelegate class]));
    }
}
