# PKLib

[![CI Status](https://img.shields.io/travis/许良伟/PKLib.svg?style=flat)](https://travis-ci.org/许良伟/PKLib)
[![Version](https://img.shields.io/cocoapods/v/PKLib.svg?style=flat)](https://cocoapods.org/pods/PKLib)
[![License](https://img.shields.io/cocoapods/l/PKLib.svg?style=flat)](https://cocoapods.org/pods/PKLib)
[![Platform](https://img.shields.io/cocoapods/p/PKLib.svg?style=flat)](https://cocoapods.org/pods/PKLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PKLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PKLib'
```

## Author

许良伟, xulw@djcars.cn

## License

PKLib is available under the MIT license. See the LICENSE file for more info.
